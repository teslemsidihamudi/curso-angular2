import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html'
})
export class SearchComponent {
  termino: String = '';
  artistas = [];

  constructor(private spotifyService: SpotifyService) { }
  searchArtist() {
    this.artistas = this.spotifyService.getArtists();
  }
}
