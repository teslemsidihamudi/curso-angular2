import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

// This service calls Spotify App, but doesn't work because no work the token.
// Must create other type of Http calls including Token like a Header.
// To continue the curse I use static data.

@Injectable()
export class SpotifyService {

  artirs: any = [];
  url: String = 'https://api.spotify.com/v1/search';

  constructor( private http: Http) { }

  getArtists() {
    let token = '-H "Accept: application/json" -H "Authorization: Bearer BQDRSvHj5GevOx-_3SsheKsT9QCkVdqHyCp-TtkyJ8-3seTE0k0pY5hk1xfeA2UYlmS2Xg_EYe-LOW19rSODNvWiVWXEz0NczGPbmLJV0qqZrSiD2tBO1IsE2YqwiFmVTm9VLralQ903NwY"';
    let query = '?q=${term}&type=artist' + token;
    let urlBusqueda = this.url + query;

    // return this.http.get(urlBusqueda)
    // .map(response => {
       let artista = {
          "items" : [ {
            "external_urls" : {
              "spotify" : "https://open.spotify.com/artist/4nDoRrQiYLoBzwC5BhVJzF"
            },
            "followers" : {
              "href" : null,
              "total" : 748072
            },
            "genres" : [ "dance pop", "pop", "post-teen pop" ],
            "href" : "https://api.spotify.com/v1/artists/4nDoRrQiYLoBzwC5BhVJzF",
            "id" : "4nDoRrQiYLoBzwC5BhVJzF",
            "images" : [ {
              "height" : 640,
              "url" : "https://i.scdn.co/image/f391d0fa675f28ad50d3041a9b1803e5e4c0a5c3",
              "width" : 640
            }, {
              "height" : 320,
              "url" : "https://i.scdn.co/image/7dce1aa42eed23fe5bba6085babc98ac760b3947",
              "width" : 320
            }, {
              "height" : 160,
              "url" : "https://i.scdn.co/image/aa9f03ea0bd3bda2683a6598361942561f7fd3aa",
              "width" : 160
            } ],
            "name" : "Camila Cabello",
            "popularity" : 93,
            "type" : "artist",
            "uri" : "spotify:artist:4nDoRrQiYLoBzwC5BhVJzF"
          }, {
            "external_urls" : {
              "spotify" : "https://open.spotify.com/artist/2gRP1Ezbtj3qrERnd0XasU"
            },
            "followers" : {
              "href" : null,
              "total" : 917749
            },
            "genres" : [ "latin", "latin arena pop", "latin pop", "reggaeton", "tropical" ],
            "href" : "https://api.spotify.com/v1/artists/2gRP1Ezbtj3qrERnd0XasU",
            "id" : "2gRP1Ezbtj3qrERnd0XasU",
            "images" : [ {
              "height" : 640,
              "url" : "https://i.scdn.co/image/3af593342cc5a7dfc867980f2d532f8369056e29",
              "width" : 640
            }, {
              "height" : 320,
              "url" : "https://i.scdn.co/image/21ecd294009ffa3be36dc25e6426b6618055ab09",
              "width" : 320
            }, {
              "height" : 160,
              "url" : "https://i.scdn.co/image/9f8ca5acc1140d206aebf1c8feb35a7086ac98a3",
              "width" : 160
            } ],
            "name" : "Camila",
            "popularity" : 73,
            "type" : "artist",
            "uri" : "spotify:artist:2gRP1Ezbtj3qrERnd0XasU"
          }, {
            "external_urls" : {
              "spotify" : "https://open.spotify.com/artist/0q5hllRooyb7Mab7YfB04k"
            },
            "followers" : {
              "href" : null,
              "total" : 45
            },
            "genres" : [ ],
            "href" : "https://api.spotify.com/v1/artists/0q5hllRooyb7Mab7YfB04k",
            "id" : "0q5hllRooyb7Mab7YfB04k",
            "images" : [ ],
            "name" : "Camila Mayor",
            "popularity" : 43,
            "type" : "artist",
            "uri" : "spotify:artist:0q5hllRooyb7Mab7YfB04k"
          }, {
            "external_urls" : {
              "spotify" : "https://open.spotify.com/artist/3VCrybIJKH7UurbDcZbMmn"
            },
            "followers" : {
              "href" : null,
              "total" : 29090
            },
            "genres" : [ "chilean rock" ],
            "href" : "https://api.spotify.com/v1/artists/3VCrybIJKH7UurbDcZbMmn",
            "id" : "3VCrybIJKH7UurbDcZbMmn",
            "images" : [ {
              "height" : 640,
              "url" : "https://i.scdn.co/image/334c21a909ba1a9ef57839a5c2e8c5bedd6b9d2f",
              "width" : 640
            }, {
              "height" : 320,
              "url" : "https://i.scdn.co/image/2df1a6368cfebd46ae8f5df007f6cc4204b0f56d",
              "width" : 320
            }, {
              "height" : 160,
              "url" : "https://i.scdn.co/image/93477664e09a281c12b261f3ecfa3682f2f44872",
              "width" : 160
            } ],
            "name" : "Camila Gallardo",
            "popularity" : 57,
            "type" : "artist",
            "uri" : "spotify:artist:3VCrybIJKH7UurbDcZbMmn"
          }, {
            "external_urls" : {
              "spotify" : "https://open.spotify.com/artist/1aMXU2xuC6FdOyD7SFdL3X"
            },
            "followers" : {
              "href" : null,
              "total" : 3068
            },
            "genres" : [ ],
            "href" : "https://api.spotify.com/v1/artists/1aMXU2xuC6FdOyD7SFdL3X",
            "id" : "1aMXU2xuC6FdOyD7SFdL3X",
            "images" : [ {
              "height" : 640,
              "url" : "https://i.scdn.co/image/87d15f1fe83f28e92e00bde46d1e523fd5a9655f",
              "width" : 640
            }, {
              "height" : 320,
              "url" : "https://i.scdn.co/image/f923e048aa0392fd5f6fa3dd9342f34eb78250e4",
              "width" : 320
            }, {
              "height" : 160,
              "url" : "https://i.scdn.co/image/09d0c702405406e5cf954402156438269e6151f4",
              "width" : 160
            } ],
            "name" : "Camila Luna",
            "popularity" : 36,
            "type" : "artist",
            "uri" : "spotify:artist:1aMXU2xuC6FdOyD7SFdL3X"
          }, {
            "external_urls" : {
              "spotify" : "https://open.spotify.com/artist/0SJy1J0FgP21lbvGBMKT8H"
            },
            "followers" : {
              "href" : null,
              "total" : 43054
            },
            "genres" : [ "chilean rock", "nueva cancion", "trova" ],
            "href" : "https://api.spotify.com/v1/artists/0SJy1J0FgP21lbvGBMKT8H",
            "id" : "0SJy1J0FgP21lbvGBMKT8H",
            "images" : [ {
              "height" : 640,
              "url" : "https://i.scdn.co/image/cd9b957bc6f45920dfc9a3e5c49e739d7fee2efb",
              "width" : 640
            }, {
              "height" : 320,
              "url" : "https://i.scdn.co/image/56d783b0921b23801ad2340f55c760e511324bc1",
              "width" : 320
            }, {
              "height" : 160,
              "url" : "https://i.scdn.co/image/845f87d883ed1f9d9aa13d25ec4a3532a99219a7",
              "width" : 160
            } ],
            "name" : "Camila Moreno",
            "popularity" : 49,
            "type" : "artist",
            "uri" : "spotify:artist:0SJy1J0FgP21lbvGBMKT8H"
          }, {
            "external_urls" : {
              "spotify" : "https://open.spotify.com/artist/0fCpd1gWP1kObFnctwl9r6"
            },
            "followers" : {
              "href" : null,
              "total" : 559
            },
            "genres" : [ ],
            "href" : "https://api.spotify.com/v1/artists/0fCpd1gWP1kObFnctwl9r6",
            "id" : "0fCpd1gWP1kObFnctwl9r6",
            "images" : [ ],
            "name" : "Camila Mendes",
            "popularity" : 48,
            "type" : "artist",
            "uri" : "spotify:artist:0fCpd1gWP1kObFnctwl9r6"
          }, {
            "external_urls" : {
              "spotify" : "https://open.spotify.com/artist/52Y9UQWlCoArmqJVFwaR2Q"
            },
            "followers" : {
              "href" : null,
              "total" : 2769
            },
            "genres" : [ ],
            "href" : "https://api.spotify.com/v1/artists/52Y9UQWlCoArmqJVFwaR2Q",
            "id" : "52Y9UQWlCoArmqJVFwaR2Q",
            "images" : [ {
              "height" : 640,
              "url" : "https://i.scdn.co/image/553ad3a88ea939a204dbb258d3a78f16bd71e965",
              "width" : 640
            }, {
              "height" : 300,
              "url" : "https://i.scdn.co/image/e69031fb6c853b0310a945ad7c925ad498d4e8fe",
              "width" : 300
            }, {
              "height" : 64,
              "url" : "https://i.scdn.co/image/d5ae3677efb9835f9340fbd45cb45f0b44bac13d",
              "width" : 64
            } ],
            "name" : "Camila Fernández",
            "popularity" : 40,
            "type" : "artist",
            "uri" : "spotify:artist:52Y9UQWlCoArmqJVFwaR2Q"
          }, {
            "external_urls" : {
              "spotify" : "https://open.spotify.com/artist/7EPzvNBtKDGLrxG3KjKwhU"
            },
            "followers" : {
              "href" : null,
              "total" : 59
            },
            "genres" : [ ],
            "href" : "https://api.spotify.com/v1/artists/7EPzvNBtKDGLrxG3KjKwhU",
            "id" : "7EPzvNBtKDGLrxG3KjKwhU",
            "images" : [ {
              "height" : 640,
              "url" : "https://i.scdn.co/image/8d6877e48a0b8794c9db7dc572a18f5e67bd3763",
              "width" : 640
            }, {
              "height" : 300,
              "url" : "https://i.scdn.co/image/b171e54101851921ec7a910182428486be7d7c62",
              "width" : 300
            }, {
              "height" : 64,
              "url" : "https://i.scdn.co/image/4146beeddb389b2a2a6da2217d33f646c99d71ae",
              "width" : 64
            } ],
            "name" : "Camila Mora",
            "popularity" : 34,
            "type" : "artist",
            "uri" : "spotify:artist:7EPzvNBtKDGLrxG3KjKwhU"
          }]
        }
        return artista.items;
    // });
  }
}
