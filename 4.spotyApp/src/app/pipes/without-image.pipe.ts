import { Pipe, PipeTransform } from '@angular/core';

/**
 * This Pipe is for artits that they haven't picture,
 * we use this pipe for don't touch the data from service
 */
@Pipe({
  name: 'withoutImage'
})
export class WithoutImagePipe implements PipeTransform {

  transform(value: any []): string {
    if (!value) {
      return 'assets/img/noimage.png';
    }
    return (value.length > 0) ? value[1].url : 'assets/img/noimage.png' ;
  }

}
