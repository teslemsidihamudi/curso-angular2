import { importType } from '@angular/compiler/src/output/output_ast';

import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { HomeComponent } from './components/home/home.component';
import { SearchComponent } from './components/search/search.component';
import { ArtistComponent } from './components/artist/artist.component';



const ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'artist/:id', component: ArtistComponent },
    { path: 'search', component: SearchComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
];


@NgModule({
    imports: [
      RouterModule.forRoot(ROUTES) // configuración para el módulo raíz
    ],
    exports: [
      RouterModule // se importará desde el módulo padre
    ]
  })
  export class AppRoutingModule { }
