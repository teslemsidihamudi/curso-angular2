import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contrasena'
})
export class ContrasenaPipe implements PipeTransform {

  transform(value: any, activar: boolean): any {
    let contrasena = '';

    if (activar) {
      for ( let i = 0; i < value.length; i++) {
        contrasena += '*';
      }

      return contrasena;
    } else {
      return value;
    }
}
}
