import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})

export class AppComponent {
  nombre = 'TEslem';
  arreglo = [1, 2, 3, 4 , 5, 6, 7, 8, 9, 10];
  title = 'app';
  pi= Math.PI;
  a: Number = 0.243;
  salario = 1234.5;
  fecha = new Date();
  nombreTeslem= 'Teslem SIDi HamUdi';
  video = '0IjnCyPpwA0';
  constrasena = 'Teslem';
  activar: Boolean= true;
  heroe = {
    nombre: 'teslem',
    clave: 'logan',
    edad: '23',
    direccion: {
      calle: 'calle',
      casa: 'X'
    }
  };
}
